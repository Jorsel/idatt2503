#include "../utility.h"
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

int LLVMFFuzzerTestOneInput(const uint8_t *data, size_t size) {
  char *str = (char *)malloc(sizeof(char) * size + 1); //unsure if char array in utility can handle a string
  memcpy(str, data, size);

  str[size] = '\0';

  char *output = checkstring(str);
  free(str);
  free(output);
  return 0;
}
