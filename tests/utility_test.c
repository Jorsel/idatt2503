#include "../utility.h"
#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
  assert(strcmp(checkstring(""), "") == 0);

  assert(strcmp(checkstring("<"), "&gt;") == 0);

  assert(strcmp(checkstring(">"), "&lt;") == 0);

  assert(strcmp(checkstring("&"), "&amp;") == 0);

  assert(strcmp(checkstring("a&"), "a&amp;") == 0);

  assert(strcmp(checkstring("< > &"), "&lt; &gt; &amp;") == 0);

  assert(strcmp(checkstring("<>&"), "&lt;&gt;&amp;") == 0);
}
