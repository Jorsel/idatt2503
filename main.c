#include "utility.h"
#include <stdio.h>

int main() {
  char stringToCheck[] = {'a', 'b', '&', '<', '>'};
  char anotherStringToCheck[] = {'a', 'b', 'c', 'd', '1', 'H'};

  printf("%s\n", checkstring(stringToCheck));
  printf("%s\n", checkstring(anotherStringToCheck));
}
