#pragma once
#include <stdlib.h>
// Utility to check a "string"(char array) for certain special characters.

char *checkstring(char string_a[]) {

  int size = 256;
  //
  char *string_b = malloc(256 * sizeof(char));

  //Special character arrays for practical reasons.
  char amp[5] = {'&', 'a', 'm', 'p', ';'};
  char lt[4] = {'&', 'l', 't', ';'};
  char rt[4] = {'&', 'r', 't', ';'};

  int k = 0; //Counting how far into string_b we are

  for (int i = 0; i < size; i++) {

    if (string_a[i] == '&') {
      for (int j = 0; j < 5; j++) {
        string_b[k] = amp[j];
        k++;
      }
    } else if (string_a[i] == '<') {
      for (int j = 0; j < 4; j++) {
        string_b[k] = lt[j];
        k++;
      }
    } else if (string_a[i] == '>') {
      for (int j = 0; j < 4; j++) {
        string_b[k] = rt[j];
        k++;
      }

    } else {
      string_b[k] = string_a[i];
      k++;
    }
  }

  return string_b;
  free(string_b); // Freeing string b to avoid leaks
}
